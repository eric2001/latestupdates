# LatestUpdates
![LatestUpdates](./screenshot.jpg) 

## Description
LatestUpdates is a plugin for [Gallery 3](http://gallery.menalto.com/) which will put two links into your sidebar. The first link will lead to a page that will display all the photos/videos in your Gallery in chronological order with the most recently uploaded items on page one and the oldest stuff on the last page. The second link will do the same thing, but limits its self to the current album and its sub-albums.

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
This installs like any other Gallery 3 module.  Download and extract it into your modules folder.  Afterwards log into the Gallery web interface and enable in under the Admin -> Modules menu.

## History
Version 1.5.0
> - Updated to take advantage of virtual album support in Gallery 3.0.3.
> - Added "Most Viewed" / "Recent Uploads" / "Recent Albums" tabs to user profile pages.
> - Released 13 June 2012.
>
> Download: [Version 1.5.0](/uploads/6471948c4a8f82434c15318ae6d567f3/latestupdates150.zip)

Version 1.4.1
> - Bug Fix:  Sometimes the photo count was wrong on the album updates view.  This has been corrected.
> - Released 26 February 2010.
>
> Download: [Version 1.4.1](/uploads/db1fd57740d811c867e4588742b71dd9/latestupdates141.zip)

Version 1.4.0
> - Display the sidebar links on all Gallery pages, not just album pages, but hide the link for "This Album" if it isn't an album.
> - Released 25 February 2010.
>
> Download: [Version 1.4.0](/uploads/29b965b74375e1b00264f65909ab1746/latestupdates140.zip)

Version 1.3.0
> - Updated for recent module API changes in Gallery 3.
> - Released 20 January 2010.
>
> Download: [Version 1.3.0](/uploads/d15e44bb92b37153e6e6033a7a3fe2d7/latestupdates130.zip)

Version 1.2.0
> - Updated to support the dynamic sidebar feature in the current version of Gallery.
> - Released on 12 November 2009.
>
> Download: [Version 1.2.0](/uploads/f88772c402d12e8f5fe4f6f0eb96553f/latestupdates120.zip)

Version 1.0.1
> - Replace p::clean with html::clean for recent Gallery API change.
> - Fixed issue that caused the sidebar block to not load properly with the current Gallery 3 git.
> - Released 31 August 2009
>
> Download: [Version 1.0.1](/uploads/3ab50a4f3da0dd512aa205eb5b313555/latestupdates101.zip)

Version 1.0.0
> - Initial Release
> - Released on 08 July 2009
>
> Download: [Version 1.0.0](/uploads/1b0c62a82e16819fabea68206c86f457/latestupdates100.zip)
